package eu.niva.orchestrator.engine.api;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.locationtech.jts.geom.Geometry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringRequest
{
	@NotNull private String polygon_id;
	@NotNull private Geometry geometry;
	@NotNull private String srs;
	@NotNull private String monitor_type_name;
	@NotNull private Integer reference_year;
	private Map<String, ?> attributes;	
}


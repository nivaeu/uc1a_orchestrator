package eu.niva.orchestrator.engine.scheduler;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.niva.orchestrator.engine.services.MonitoringService;

public class SchedulingJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(SchedulingJob.class);
    
    public SchedulingJob() {
	super();
	// TODO Auto-generated constructor stub
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	Map<String, Object> wm = dataMap.getWrappedMap();	
	MonitoringService monitoringService = (MonitoringService) wm.get("monitoringService");
	try {
	    monitoringService.scheduleMonitorTypesJobs();
	} catch (Exception e) {
	   log.error("Error on scheduling monitoryng types on new year", e);
	}
    }

}

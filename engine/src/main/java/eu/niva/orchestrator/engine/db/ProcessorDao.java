package eu.niva.orchestrator.engine.db;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Optional;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import eu.niva.orchestrator.engine.db.ntt.Processor;

public interface ProcessorDao extends Transactional<ProcessorDao> {

    @SqlUpdate("INSERT INTO orc_processor (processor_name, connector_name, processor_params) VALUES (:processor_name, :connector_name, :processor_params)")
    public void insertProcessor(@BindBean Processor processor);

    @GetGeneratedKeys("processor_id")
    @SqlUpdate("DELETE FROM orc_processor WHERE processor_name=:procName")
    public Integer deleteProcessor(@Bind("procName") String procName);

    @GetGeneratedKeys("processor_id")
    @SqlUpdate("UPDATE orc_processor SET connector_name=:connector_name, processor_params=:processor_params WHERE processor_name=:processor_name")
    public Integer updateProcessor(@BindBean Processor processor);

    @RegisterBeanMapper(Processor.class)
    @SqlQuery("SELECT * FROM orc_processor ORDER BY processor_name")
    public List<Processor> getProcessors();

    @RegisterBeanMapper(Processor.class)
    @SqlQuery("SELECT * FROM orc_processor WHERE processor_name=:procName")
    Optional<Processor> findProcessorByName(@Bind("procName") String procName);

    @SqlQuery("SELECT monitor_type_name FROM orc_monitor_type_processors WHERE processor_name=:processor_name")
    public List<String> getProcessorMonitoringTypes(@Bind("processor_name") String processor_name);

}

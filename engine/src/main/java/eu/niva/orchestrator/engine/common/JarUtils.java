package eu.niva.orchestrator.engine.common;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JarUtils
{

	private static final Logger log = LoggerFactory.getLogger(JarUtils.class);

	public static void buildJar(String workflowName) throws Exception
	{

		try (JarOutputStream target = new JarOutputStream(new FileOutputStream("wf-jar/" + workflowName + ".jar")))
		{
			File basePath = new File("wf-jar");
			add(new File("wf-jar/groovy"), basePath, target);
			add(new File("wf-jar/abaco-workflow.json"), basePath, target);
			target.close();
		}
	}

	private static void add(File source, File base, JarOutputStream target) throws Exception
	{
		BufferedInputStream in = null;

		try
		{
			if (!source.exists())
			{
				throw new IOException("Source directory is empty");
			}
			if (source.isDirectory())
			{
				for (File nestedFile : source.listFiles())
				{
					add(nestedFile, base, target);
				}
				return;
			}

			String entryName = base.toPath().relativize(source.toPath()).toFile().getPath().replace("\\", "/");
			JarEntry entry = new JarEntry(entryName);
			entry.setTime(source.lastModified());
			target.putNextEntry(entry);
			in = new BufferedInputStream(new FileInputStream(source));

			byte[] buffer = new byte[1024];
			while (true)
			{
				int count = in.read(buffer);
				if (count == -1)
					break;
				target.write(buffer, 0, count);
			}
			target.closeEntry();
		}
		catch (Exception e)
		{
			log.error("Error creating monitoring workflow Jar file");
			throw e;
		}
		finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				}
				catch (Exception ignored)
				{
					throw new RuntimeException(ignored);
				}
			}
		}
	}

}

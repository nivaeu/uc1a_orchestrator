package eu.nive.orchestrator.engine.test;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import eu.niva.orchestrator.engine.OrchestratorApplication;
import eu.niva.orchestrator.engine.OrchestratorConfiguration;
import io.dropwizard.setup.Environment;

public class OrchestratorApplicationTest extends OrchestratorApplication {

	@Override
	public void run(OrchestratorConfiguration config, Environment environment) throws Exception {		
	}


}

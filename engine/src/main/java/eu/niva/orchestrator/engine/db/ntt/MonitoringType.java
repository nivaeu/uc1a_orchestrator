package eu.niva.orchestrator.engine.db.ntt;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringType {
	
	private Integer monitor_type_id;
	private Short is_immediate;
	private Integer frequency;
	private String monitor_type_name;
	private Integer doy_start;
	private Integer doy_end;
	private String workflow;
	private String workflow_code;
}

# NIVA - uc1a orchestrator SDK

This repo contains the SDK library to create orchestrator's packagesbackend project for the NIVA uc1a orchestrator.

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Compile and package

To create the libraries and the engine api artifact:

    mvn clean install
    
## The Connector interface
In order for the workflow engine to be able to execute the developed algorithm, an implementation of `eu.niva.orchestrator.sdk.Connector` must be created; this implementation is in charge of invokink the
algorithm for a given polygon at a given date.

The `Connector` returns the result of the algorithm as a java object.

package eu.niva.orchestrator.engine.api;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WorkflowOrchestrator {
	private String code;
	private String version;
	private List<Procedure> procedures;
	private List<Node> nodes;
	private List<Transition> transitions;

	@Data
	public
	static class Procedure {
		private String processor;
		private String node_name;
	}

	
	@Data
	public static class Node{
	    public String type;
	    public String name;
	    public Map<String, Object> metadata;
	}


	@Data
	public static class Transition {
		private String node_from;
		private String node_to;
		private Integer is_default;
		private String description;
		private List<TranRule> tran_rules;
	}

	@Data
	public static class TranRule {
		private String expression;
	}
	
}

package eu.niva.orchestrator.engine.jdbi.argument;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Connection;
import java.sql.Types;
import java.util.function.Consumer;

import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.config.ConfigRegistry;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTWriter;
import org.postgis.PGgeometry;

public class PgGeometryArgumentFactory extends AbstractArgumentFactory<Geometry>
{
	private Consumer<Connection> geometrySupportConsumer;
	
	private static final int SQL_TYPE = Types.JAVA_OBJECT;
	private WKTWriter wktWriter = new WKTWriter();

	public PgGeometryArgumentFactory(Consumer<Connection> geometrySupportConsumer)
	{
		super(SQL_TYPE);
		this.geometrySupportConsumer = geometrySupportConsumer;
	}

	@Override
	protected Argument build(Geometry value, ConfigRegistry config)
	{
		return (position, statement, ctx) -> {
			if (value == null || value.isEmpty())
			{
				statement.setNull(position, SQL_TYPE);
				return;
			}
			
			geometrySupportConsumer.accept(statement.getConnection());
			
			String wkt = wktWriter.write(value);
			if (value.getSRID() != 0)
				wkt = "SRID=" + value.getSRID() + ";" + wkt;
			
			statement.setObject(position, new PGgeometry(wkt));
		};
	}

}

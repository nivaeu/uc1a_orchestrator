package eu.niva.orchestrator.engine.workflow;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WorkflowDto {
	private String code;
	private String version;
	private String md5;
	private List<Procedure> procedures = new ArrayList<>();
	private List<Rule> rules = new ArrayList<>();
	private List<Node> nodes;
	private List<Transition> transitions;
	private List<Variable> variables = new ArrayList<>();

	@Data
	public static class Procedure {
		private String class_name;
		private String node_name;
	}

	@Data
	public static class Fact {
		private String fact_name;
		private String fact_type;
		private Integer is_list = 0;
	}

	@Data
	public static class Rule {
		private String drools_file;
		private String node_name;
		private List<Fact> facts;
	}
	
	@Data
	public static class Node{
	    public String type;
	    public String name;
	    public Map<String, Object> metadata;
	}

	@Data
	public static class Scope {
		private String scope;
	}

	@Data
	public static class Transition {
		private String node_from;
		private String node_to;
		private Integer is_default = 0;
		private String description;
		private List<TranRule> tran_rules = new ArrayList<>();
		private List<Scope> scopes = new ArrayList<>();
	}

	@Data
	public static class TranRule {
		private String expression;
	}
	
	@Data
	@AllArgsConstructor
	public static class Variable {
		private String name;
		private String type;
		private Integer is_list;
		private Integer fl_persist = 1;
	}
}

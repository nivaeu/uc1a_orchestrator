import java.util.Map;
import com.abacogroup.workflow.sdk.beans.ProcessData;
import eu.niva.orchestrator.sdk.*;
import com.abacogroup.orchestrator.test.AgriculturalActivities;

public class AgriculturalActivities implements Procedure {
  public void execute(ProcessData processData) throws Exception {
	  com.abacogroup.orchestrator.test.AgriculturalActivities connector = new com.abacogroup.orchestrator.test.AgriculturalActivities();
	  MonitoringParams monitoringParams = (MonitoringParams) processData.getGlobalVars().get("monitoringParams");
	  Object ret = connector.compute(monitoringParams, processData.getGlobalVars().get("processorParams"));	
	  processData.getGlobalVars().put("AgriculturalActivities", ret);
  }
}
package eu.niva.orchestrator.sdk;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.Map;

import org.locationtech.jts.geom.Geometry;

/**
 * This class models the monitoring parameters
 */
public class MonitoringParams {

	private String polygonId;

	private Geometry geometry;

	private Integer year;

	private OffsetDateTime pointInTime;

	private Map<String, Object> attributes;

	public MonitoringParams() {
	}

	public MonitoringParams(String polygonId, Geometry geometry, Integer year, OffsetDateTime pointInTime, Map<String, Object> attributes) {
		this.polygonId = polygonId;
		this.geometry = geometry;
		this.year = year;
		this.pointInTime = pointInTime;
		this.attributes = attributes;
	}

	/**
	 * @return the current polygon id
	 */
	public String getPolygonId() {
		return polygonId;
	}

	/**
	 * @return the polygon geometry
	 */
	public Geometry getGeometry() {
		return geometry;
	}

	/**
	 * @return the monitoring year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @return the point in time to compute
	 */
	public OffsetDateTime getPointInTime() {
		return pointInTime;
	}

	/**
	 * @return the additional attributes, if any
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	
}

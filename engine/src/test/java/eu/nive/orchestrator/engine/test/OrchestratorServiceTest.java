package eu.nive.orchestrator.engine.test;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.orchestrator.engine.api.MonitoringRequest;
import eu.niva.orchestrator.engine.common.JarUtils;
import eu.niva.orchestrator.engine.db.MonitoringDao;
import eu.niva.orchestrator.engine.db.ProcessorDao;
import eu.niva.orchestrator.engine.db.ntt.MonitoringType;
import eu.niva.orchestrator.engine.db.ntt.Processor;
import eu.niva.orchestrator.engine.services.MonitoringService;
import eu.niva.orchestrator.engine.services.ProcessorService;

class OrchestratorServiceTest {

    private ProcessorDao processorDao;
    private MonitoringDao monitoringDao;
    private MonitoringService monitoringService;
    private ProcessorService processorService;

    @BeforeEach
    void setUp() throws Exception {
	monitoringDao = mock(MonitoringDao.class);
	processorDao = mock(ProcessorDao.class);
	monitoringService = new MonitoringService(processorDao, monitoringDao, new ObjectMapper(), null, null);
	processorService = new ProcessorService(processorDao, monitoringService, new ObjectMapper());
    }

    @Test
    public void whenDeleteProcessor_butIsInUse_thenThrowException() {

	List<String> monTypeList = new ArrayList<String>();
	monTypeList.add("montype1");
	when(processorDao.getProcessorMonitoringTypes(any())).thenReturn(monTypeList);
	when(processorDao.findProcessorByName(any())).thenReturn(Optional.of(new Processor()));
	Exception exception = assertThrows(WebApplicationException.class, () -> {
	    processorService.deleteProcessor("NumberOfPixels");
	});

	String actualMessage = exception.getMessage();
	assertTrue(actualMessage.contains("Cannot delete processor while is in use"));
    }

    @Test
    public void whenCreateMonitoringRequest_thenReturn200_OK() {

	MonitoringType monitorType = new MonitoringType();
	monitorType.setIs_immediate((short) 0);
	when(monitoringDao.getMonitoringTypeByName(any())).thenReturn(Optional.of(monitorType));

	MonitoringRequest monitoringRequest = new MonitoringRequest();
	GeometryFactory gf = new GeometryFactory();
	Polygon geo = gf.createPolygon(
	    new Coordinate[] { new Coordinate(-9, -2), new Coordinate(-9, -1), new Coordinate(-8, -1), new Coordinate(-8, -2), new Coordinate(-9, -2) });
	monitoringRequest.setGeometry(geo);
	monitoringRequest.setSrs("srsValue");
	monitoringRequest.setReference_year(2021);
	monitoringRequest.setPolygon_id("id13");
	monitoringRequest.setMonitor_type_name("monitor_type_name4");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("attr1", 1);
	map.put("attr2", "val2");
	monitoringRequest.setAttributes(map);
	Response Response = monitoringService.createMonitoringRequest(monitoringRequest);
	assertTrue(Response.getStatus() == 200);

    }

    @Test
    public void whenBuildJar_thenCreateFileSuccesfully() throws Exception {
	File original = new File("src/test/resources/abaco-workflow.json");
	File copied = new File("wf-jar/abaco-workflow.json");
	FileUtils.copyFile(original, copied);
	File jarFile = new File("wf-jar/WFTEST.jar");
	if (jarFile.exists()) {
	    jarFile.delete();
	}
	JarUtils.buildJar("WFTEST");
	assertTrue(jarFile.exists());

    }
}

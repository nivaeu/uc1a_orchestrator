package eu.niva.orchestrator.sdk;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Map;

/**
 * A connector implementation links the orchestrator to an "algorithm" and is responsible to invoke the
 * algorithm as needed. 
 */
public interface Connector {

	/**
	 * Executes the algorithm.
	 * 
	 * @param monitoringParams the current monitoring parameters
	 * @param processorParams the prcessor specific parameters
	 * @return the computation result
	 * @throws Exception any exceptions
	 */
	Object compute(MonitoringParams monitoringParams, Map<String, Object> processorParams) throws Exception;
}

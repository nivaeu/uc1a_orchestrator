package eu.niva.orchestrator.engine.resources;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

import eu.niva.orchestrator.engine.api.ProcessorRequest;
import eu.niva.orchestrator.engine.services.ProcessorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/api/v1/processor")
@Timed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessorResources {
	
	private ProcessorService processorService;

	public ProcessorResources(ProcessorService processorService) {
		this.processorService = processorService;
	}


	@POST
	@ApiResponse(responseCode = "200", description = "processor created succesfully")
	@Operation(description = "Monitoring processor creation")
	public Response createProcessor(@Valid @NotNull ProcessorRequest processor) throws Exception {
		return processorService.createProcessor(processor);
	}
	
	
	@DELETE
	@Path("{processorName}")
	@ApiResponse(responseCode = "200", description = "processor deleted succesfully")
	@Operation(description = "Monitoring processor delete")
	public Response deleteProcessor(@NotNull @PathParam("processorName") String processorName) throws Exception {
		return processorService.deleteProcessor(processorName);
	}
	
	@GET
	@ApiResponse(responseCode = "200", description = "processors retrieved succesfully")
	@Operation(description = "Monitoring processor listing")
	public List<ProcessorRequest> getProcessors() throws Exception {
		return processorService.getProcessors();
	}
	
	@PUT
	@ApiResponse(responseCode = "200", description = "processor updated succesfully")
	@Operation(description = "Monitoring processor update")
	public Response updateProcessor(@Valid @NotNull ProcessorRequest processor) throws Exception {
		return processorService.updateProcessor(processor);
	}
}

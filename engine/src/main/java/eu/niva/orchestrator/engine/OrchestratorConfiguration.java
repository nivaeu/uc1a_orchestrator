package eu.niva.orchestrator.engine;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;

public class OrchestratorConfiguration extends Configuration {

    @Valid
    @NotNull
    private DataSourceFactory database;

    @NotNull
    private String workflowRestProcessUrl;

    @NotNull
    private String workflowRestUploadUrl;

    @NotNull
    private boolean enableSqlLogging;

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
	this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
	return database;
    }

    public boolean isEnableSqlLogging() {
	return enableSqlLogging;
    }

    public void setEnableSqlLogging(boolean enableSqlLogging) {
	this.enableSqlLogging = enableSqlLogging;
    }

    public String getWorkflowRestProcessUrl() {
	return workflowRestProcessUrl;
    }

    public void setWorkflowRestProcessUrl(String workflowRestProcessUrl) {
	this.workflowRestProcessUrl = workflowRestProcessUrl;
    }

    public String getWorkflowRestUploadUrl() {
	return workflowRestUploadUrl;
    }

    public void setWorkflowRestUploadUrl(String workflowRestUploadUrl) {
	this.workflowRestUploadUrl = workflowRestUploadUrl;
    }

    @Valid
    @NotNull
    private JerseyClientConfiguration jerseyClient = new JerseyClientConfiguration();

    @JsonProperty("jerseyClient")
    public JerseyClientConfiguration getJerseyClientConfiguration() {
	return jerseyClient;
    }

    @JsonProperty("jerseyClient")
    public void setJerseyClientConfiguration(JerseyClientConfiguration jerseyClient) {
	this.jerseyClient = jerseyClient;
    }
}

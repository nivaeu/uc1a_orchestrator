package eu.niva.orchestrator.engine.resources;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

import eu.niva.orchestrator.engine.api.MonitoringRequest;
import eu.niva.orchestrator.engine.api.MonitoringResult;
import eu.niva.orchestrator.engine.api.MonitoringResultList;
import eu.niva.orchestrator.engine.api.MonitoringTypeRequest;
import eu.niva.orchestrator.engine.api.WorkflowOrchestrator;
import eu.niva.orchestrator.engine.db.ntt.MonitoringType;
import eu.niva.orchestrator.engine.services.MonitoringService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/api/v1/monitoring")
@Timed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MonitoringResources {
    private MonitoringService monitoringService;

    public MonitoringResources(MonitoringService monitorService) {
	this.monitoringService = monitorService;
    }

    @POST
    @ApiResponse(responseCode = "200", description = "monitoring request created succesfully")
    @Operation(description = "Monitoring request creation")
    public Response createMonitoringRequest(@Valid @NotNull MonitoringRequest monitoringRequest) throws Exception {
	return monitoringService.createMonitoringRequest(monitoringRequest);
    }

    @POST
    @Path("/type")
    @ApiResponse(responseCode = "200", description = "monitoring type created succesfully")
    @Operation(description = "Monitoring Type creation")
    public Response createMonitoringType(@Valid @NotNull MonitoringTypeRequest monitoringTypeRequest) throws Exception {
	return monitoringService.createMonitoringType(monitoringTypeRequest);
    }

    @PUT
    @Path("/type")
    @ApiResponse(responseCode = "200", description = "monitoring type updated succesfully")
    @Operation(description = "Monitoring Type update")
    public Response updateMonitoringType(@Valid @NotNull MonitoringTypeRequest monitoringTypeRequest) throws Exception {
	return monitoringService.updateMonitoringType(monitoringTypeRequest);
    }

    @DELETE
    @Path("/type/{monitorTypeName}")
    @ApiResponse(responseCode = "200", description = "monitoring type deleted succesfully")
    @Operation(description = "Monitoring Type delete")
    public Response deleteMonitoringType(@NotNull @PathParam("monitorTypeName") String monitorTypeName) throws Exception {
	return monitoringService.deleteMonitoringType(monitorTypeName);
    }

    @GET
    @Path("/result/list/{registrationId}")
    @ApiResponse(responseCode = "200", description = "monitoring result list retrieved succesfully")
    @Operation(description = "Get result list for registration Id, retrieve execution dates and result Ids")
    public MonitoringResultList getMonitoringResultList(@NotNull @PathParam("registrationId") long registrationId) throws Exception {
	return monitoringService.getMonitoringResultList(registrationId);
    }

    @GET
    @ApiResponse(responseCode = "200", description = "monitoring result retrieved succesfully")
    @Operation(description = "Get monitoring result for result Id")
    @Path("/result/{resultId}")
    public MonitoringResult getMonitoringResult(@NotNull @PathParam("resultId") long resultId) throws Exception {
	return monitoringService.getMonitoringResult(resultId);
    }

    @GET
    @Path("/type")
    @ApiResponse(responseCode = "200", description = "monitoring types retrieved succesfully")
    @Operation(description = "Monitoring type listing")
    public List<MonitoringType> getMonitorTypes() throws Exception {
	return monitoringService.getMonitoringTypes();
    }

    @GET
    @Path("/type/{monitorTypeName}/workflow")
    @ApiResponse(responseCode = "200", description = "monitoring type retrieved succesfully")
    @Operation(description = "Monitoring type find by name")
    public WorkflowOrchestrator getMonitorTypeWorkflow(@NotNull @PathParam("monitorTypeName") String monitorTypeName) throws Exception {
	return monitoringService.getMonitoringTypeWorkflow(monitorTypeName);
    }

//    @GET // test scope
//    @Path("/schedule")
//    public void schedule() throws Exception {
//	monitoringService.scheduleMonitorTypesJobs();
//
//    }

}

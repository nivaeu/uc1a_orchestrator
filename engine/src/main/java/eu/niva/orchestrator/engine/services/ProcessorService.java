package eu.niva.orchestrator.engine.services;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.orchestrator.engine.api.MonitoringTypeRequest;
import eu.niva.orchestrator.engine.api.ProcessorRequest;
import eu.niva.orchestrator.engine.api.WorkflowOrchestrator;
import eu.niva.orchestrator.engine.db.ProcessorDao;
import eu.niva.orchestrator.engine.db.ntt.Processor;

public class ProcessorService {

    private ProcessorDao processorDao;
    private MonitoringService monitoringService;
    private ObjectMapper objectMapper;
    private ModelMapper modelMapper;

    private static final Logger log = LoggerFactory.getLogger(ProcessorService.class);

    public ProcessorService(ProcessorDao processorDao, MonitoringService monitoringService, ObjectMapper objectMapper) {
	super();
	this.processorDao = processorDao;
	this.monitoringService = monitoringService;
	this.objectMapper = objectMapper;
	this.modelMapper = new ModelMapper();
    }

    public Response createProcessor(ProcessorRequest processorRequest) {

	try {
	    Optional<Processor> proc = processorDao.findProcessorByName(processorRequest.getProcessor_name());
	    if (proc.isPresent()) {
		throw new WebApplicationException("Processor name already exists", Response.Status.NOT_ACCEPTABLE);
	    }
	    ModelMapper modelMapper = new ModelMapper();
	    Processor processor = modelMapper.map(processorRequest, Processor.class);
	    String jsonParams = objectMapper.writeValueAsString(processorRequest.getProcessor_params());
	    processor.setProcessor_params(jsonParams);
	    processorDao.insertProcessor(processor);
	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while creating processor %s ", processorRequest.getProcessor_name());
	    log.error(message, e);
	    throw new WebApplicationException("Processor creation failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return Response.ok("Processor created succesflully").build();

    }

    public Response deleteProcessor(String procName) {
	
	Optional<Processor> proc = processorDao.findProcessorByName(procName);
	if (!proc.isPresent()) {
	    throw new NotFoundException(String.format("Processor name not found: %s", procName));
	}

	List<String> monTypes = processorDao.getProcessorMonitoringTypes(procName);
	if (!monTypes.isEmpty()) {
	    String message = "";
	    for (String name : monTypes) {
		message += " " + name;
	    }
	    throw new WebApplicationException(String.format("Cannot delete processor while is in use by %s monitoring types", message),
		Response.Status.NOT_ACCEPTABLE);
	}

	try {
	    processorDao.deleteProcessor(procName);
	} catch (Exception e) {
	    String message = String.format("Exception while deleting processor %s ", procName);
	    log.error(message, e);
	    throw new WebApplicationException("Processor delete failed", Response.Status.INTERNAL_SERVER_ERROR);
	}

	return Response.ok("Processor deleted succesflully").build();

    }

    public Response updateProcessor(ProcessorRequest processorRequest) {

	boolean updateWorkflow = false;
	Processor processor = processorDao.findProcessorByName(processorRequest.getProcessor_name())
	    .orElseThrow(() -> new NotFoundException(String.format("Processor name not found: %s", processorRequest.getProcessor_name())));
	if (!processor.getConnector_name().equals(processorRequest.getConnector_name())) {
	    updateWorkflow = true;
	}
	try {
	    ModelMapper modelMapper = new ModelMapper();
	    processor = modelMapper.map(processorRequest, Processor.class);
	    String jsonParams = objectMapper.writeValueAsString(processorRequest.getProcessor_params());
	    processor.setProcessor_params(jsonParams);
	    processorDao.updateProcessor(processor);

	    if (updateWorkflow) {// rebuild all workflows related to processor
		List<String> monTypeNames = processorDao.getProcessorMonitoringTypes(processorRequest.getProcessor_name());
		for (String monTypeName : monTypeNames) {
		    MonitoringTypeRequest monitoringTypeRequest = new MonitoringTypeRequest();
		    WorkflowOrchestrator workflowOrchestrator = monitoringService.getMonitoringTypeWorkflow(monTypeName);
		    monitoringTypeRequest.setMonitor_type_name(monTypeName);
		    monitoringTypeRequest.setWorkflow(workflowOrchestrator);
		    monitoringService.createWorkflowJar(monitoringTypeRequest);
		}
	    }
	} catch (Exception e) {
	    String message = String.format("Exception while updating processor %s ", processorRequest.getProcessor_name());
	    log.error(message, e);
	    throw new WebApplicationException("Processor update failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return Response.ok("Processor updated succesflully").build();

    }

    @SuppressWarnings("unchecked")
    public List<ProcessorRequest> getProcessors() {
	List<ProcessorRequest> processorRequestList = new ArrayList<ProcessorRequest>();
	try {
	    List<Processor> processors = processorDao.getProcessors();
	    for (Processor processor : processors) {
		ProcessorRequest processorRequest = modelMapper.map(processor, ProcessorRequest.class);
		Map<String, Object> procParamsMap = objectMapper.readValue(processor.getProcessor_params(), Map.class);
		processorRequest.setProcessor_params(procParamsMap);
		processorRequestList.add(processorRequest);
	    }
	} catch (Exception e) {
	    log.error("Exception while getting processors", e);
	    throw new WebApplicationException("Getting processors failed", Response.Status.INTERNAL_SERVER_ERROR);
	}

	return processorRequestList;

    }
}

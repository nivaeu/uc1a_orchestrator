package eu.niva.orchestrator.engine.db;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Optional;

import org.jdbi.v3.core.result.ResultIterator;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import eu.niva.orchestrator.engine.db.ntt.MonitoringReq;
import eu.niva.orchestrator.engine.db.ntt.MonitoringRes;
import eu.niva.orchestrator.engine.db.ntt.MonitoringType;

public interface MonitoringDao extends Transactional<MonitoringDao> {

    @GetGeneratedKeys("registration_id")
    @SqlUpdate("INSERT INTO orc_monitoring_request (polygon_id, geometry, srs, monitor_type_name, reference_year, attributes) VALUES (:polygon_id, :geometry, :srs, :monitor_type_name, :reference_year, :attributes)")
    public Long insertMonitoring(@BindBean MonitoringReq monitoringReq);

    @GetGeneratedKeys("monitor_type_id")
    @SqlUpdate("INSERT INTO orc_monitoring_type (is_immediate, doy_start, doy_end, frequency, monitor_type_name, workflow, workflow_code) VALUES (:is_immediate, :doy_start, :doy_end, :frequency, :monitor_type_name, :workflow, :workflow_code)")
    public Integer insertMonitoringType(@BindBean MonitoringType monitoringType);

    @RegisterBeanMapper(MonitoringType.class)
    @SqlQuery("SELECT * FROM orc_monitoring_type WHERE monitor_type_name=:monitor_type_name")
    public Optional<MonitoringType> getMonitoringTypeByName(@Bind("monitor_type_name") String monitor_type_name);

    @RegisterBeanMapper(MonitoringType.class)
    @SqlQuery("SELECT monitor_type_id,is_immediate,doy_start,doy_end,frequency,monitor_type_name,workflow_code FROM orc_monitoring_type ORDER BY monitor_type_id")
    public List<MonitoringType> getMonitoringTypes();

    @GetGeneratedKeys("monitor_type_id")
    @SqlUpdate("UPDATE orc_monitoring_type SET is_immediate=:is_immediate, doy_start=:doy_start, doy_end=:doy_end, frequency=:frequency WHERE monitor_type_name=:monitor_type_name")
    public Integer updateMonitoringType(@BindBean MonitoringType monitoringType);

    @GetGeneratedKeys("monitor_type_id")
    @SqlUpdate("DELETE FROM orc_monitoring_type WHERE monitor_type_name=:monitor_type_name")
    public Integer deleteMonitoringType(@Bind("monitor_type_name") String monitor_type_name);

    @RegisterBeanMapper(MonitoringReq.class)
    @SqlQuery("SELECT * FROM orc_monitoring_request WHERE registration_id=:registration_id")
    public Optional<MonitoringReq> getMonitoringRequestsByRegistrationId(@Bind("registration_id") Long registration_id);

    @RegisterBeanMapper(MonitoringReq.class)
    @SqlQuery("SELECT * FROM orc_monitoring_request WHERE monitor_type_name=:monitor_type_name")
    public List<MonitoringReq> getMonitoringRequestsByTypeName(@Bind("monitor_type_name") String monitor_type_name);

    @SqlUpdate("INSERT INTO orc_monitoring_result(registration_id, result, metadata)VALUES(:registration_id, :result, :metadata)")
    public void insertMonitoringResult(@BindBean MonitoringRes monitoringResult);

    @RegisterBeanMapper(MonitoringRes.class)
    @SqlQuery("SELECT * FROM orc_monitoring_result WHERE registration_id=:registration_id ORDER BY execution_date DESC")
    public List<MonitoringRes> getMonitoringResultList(@Bind("registration_id") long registrationId);

    @RegisterBeanMapper(MonitoringRes.class)
    @SqlQuery("SELECT * FROM orc_monitoring_result WHERE result_id=:result_id")
    public Optional<MonitoringRes> getMonitoringResult(@Bind("result_id") long resultId);

    @GetGeneratedKeys("registration_id")
    @SqlUpdate("UPDATE orc_monitoring_request SET status=:status WHERE registration_id=:registration_id")
    public Long updateStatusMonitoringRequest(@Bind("registration_id") Long registrationId, @Bind("status") String status);

    @RegisterBeanMapper(MonitoringReq.class)
    @SqlQuery("SELECT * FROM orc_monitoring_request WHERE status=:monitor_status AND reference_year=:ref_year")
    public ResultIterator<MonitoringReq> getMonitoringRequestsByStatusAndYear(@Bind("monitor_status") String monitor_status, @Bind("ref_year") int ref_year);

    @SqlUpdate("INSERT INTO orc_monitor_type_processors(monitor_type_name, processor_name)VALUES(:monitor_type_name, :processor_name)")
    public void insertMonitoringTypeProcessors(@Bind("monitor_type_name") String monitor_type_name, @Bind("processor_name") String processor_name);

    @SqlUpdate("DELETE FROM orc_monitor_type_processors WHERE monitor_type_name=:monitor_type_name")
    public void deleteMonitoringTypeProcessors(@Bind("monitor_type_name") String monitor_type_name);

}

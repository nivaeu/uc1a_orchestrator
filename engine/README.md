# NIVA - uc1a orchestrator SDK

This repo contains the create orchestrator's engine.

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Compile and package

To create the libraries and the engine api artifact:

    mvn clean package

## Pre requisites

The software has a runtime dependency on uc1a_workflow and a postgresql dabatase with the postgis extension.

## Configuration

A configuration file is to be provided as part of the the startup command; you can refer to `config.yml`
for a list of needed variables to be configured.

## Run the application

You need to first prepare the dabase schema, 
running: `java -jar target/orchestrator-engine-1.0.0.jar db migrate config.yml`

To start the engine, run: `java -jar target/orchestrator-engine-1.0.0.jar server config.yml`

You will then be able to access the openapi json schema at `/openapi.json`

## APIs

### Pre-requisites
The first thing to be done is to implement some `Connector`s using the orchestrator-sdk APIs; please refer to that module's README.md file and javadoc.


The created `Connector`s must then be put in the uc1a_workflow application, together with the orchestrator SDK jar itself (in $WORKFLOW_PATH/libs).

### Processors/procedures
A `Processor` or `Procedure` decouples the algorithm invocation, performed by a `Connector` fro mhte parameters that must be passed to the algorithm; you can have multiple `Porcessor`s for a single `Connector`, each one passing different parameters.


To register a processor you must call the following API:

```
POST /orchestrator/api/v1/processor

{
  "processor_name" : "NumberOfPixels",
  "connector_name" : "CONN_NUM_PIXELS",
  "processor_params" : {
    "modCount" : 0,
    "size" : 1,
    "threshold" : 0.5
  }
}
```

### Monitoring types
A monitoring type is defined as a workflow composed of `Processor`s nodes; each node is connected to the next ones through transitions and avery transition can have one or more rules to allow the workflow engine to understand which branch it nedds to take to move forward.

To register a monitoring type you must call the following API:

```
POST /orchestrator/api/v1/monitoring/type

{
  "frequency": 7,
  "monitor_type_name": "monitor_type_name1",
  "doy_start": 133,
  "doy_end": 135,
  "is_immediate": false,
  "workflow": {
    "code": "WFTEST_C",
    "version": "020",
    "procedures": [{
        "node_name": "NumberOfPixels",
        "processor": "NumberOfPixels"
      }, {
        "node_name": "LandUseHomogeneity",
        "processor": "LandUseHomogeneity"
      }, {
        "node_name": "AgriculturalActivities",
        "processor": "AgriculturalActivities"
      }
    ],
    "nodes": [{
        "type": "start",
        "name": "nodeStart",
        "metadata": {}
      }, {
        "type": "proc",
        "name": "NumberOfPixels",
        "metadata": {}
      }, {
        "type": "proc",
        "name": "LandUseHomogeneity",
        "metadata": {}
      }, {
        "type": "proc",
        "name": "AgriculturalActivities",
        "metadata": {}
      }, {
        "type": "end",
        "name": "nodeEndNOP",
        "metadata": {
          "outcome": "yellow",
          "message": "not enough pixel data"
        }
      }, {
        "type": "end",
        "name": "nodeEndLUH",
        "metadata": {
          "outcome": "yellow",
          "message": "non homogeneous field"
        }
      }, {
        "type": "end",
        "name": "nodeEnd",
        "metadata": {
          "outcome": "GREEN",
          "message": "ALL OK"
        }
      }
    ],
    "transitions": [{
        "node_from": "nodeStart",
        "node_to": "NumberOfPixels",
        "description": "string",
        "tran_rules": [],
        "is_default": 0
      }, {
        "node_from": "NumberOfPixels",
        "node_to": "LandUseHomogeneity",
        "description": "string",
        "tran_rules": [{
            "expression": "NumberOfPixels.key2 == 'Hello connector'"
          }
        ],
        "is_default": 0
      }, {
        "node_from": "NumberOfPixels",
        "node_to": "nodeEndNOP",
        "description": "string",
        "is_default": 1,
        "tran_rules": []
      }, {
        "node_from": "LandUseHomogeneity",
        "node_to": "AgriculturalActivities",
        "description": "string",
        "tran_rules": [],
        "is_default": 0
      }, {
        "node_from": "LandUseHomogeneity",
        "node_to": "nodeEndLUH",
        "description": "string",
        "is_default": 1,
        "tran_rules": []
      }, {
        "node_from": "AgriculturalActivities",
        "node_to": "nodeEnd",
        "description": "string",
        "is_default": 1,
        "tran_rules": []
      }
    ]
  }
}

```

The attributes are:
* frequency: the monitoring frequency in days
* doy_start: the day of the year at which the monitoring must start
* doy_end: the day of the year at which the monitoring must end
* fl_immediate: if true, the monitoring is fired immediatelly
* workflow: the workflow definition

The workflow must have a code and a version; the version must change every time the definition changes.
The workflow definition is composed of these sections:
* procedures: the procedures/processors that must be used by this monitiring type
* nodes: the workflow nodes, each node can be one of:
    * start: the start node, only one element of this type must exist in the definition
    * proc: a processor/procedure node, the node name must be the same as `node_name` on the procedures section
    * end: a terminal node; the metadata of this node will be part of the monitoring result
* transitions: the transitions between workflow nodes; each element has teh following attributes:
    * node_from: the transision's starting node
    * node_to the transision's ending node
    * description: a description of the transition
    * is_default: 0 or 1; one of the transitions leaving a node must be set as `is_default: 1` to guarantee that the workflow can proceed untill an end node is reached
    * tran_rules: a list of rules to be computed to conditionally choose a transition between the possibly many starting at the same node.
    
#### Transition rules
The transition rules are written as java expressions; all outcomes of visited nodes can be used in the form of `<processor>.<atribute>`; as an example, this hypotetic processor named NumberOfPixels:

```java
public class NumberOfPixelsProcessor implements Procedure {
  public void execute(ProcessData processData) throws Exception {
    return Map.of("numer", 10);
  }
}
```

Can be used in an tranistion rule like:

```json
"tran_rules": [{
    "expression": "NumberOfPixels.number > 5"
  }
],
```

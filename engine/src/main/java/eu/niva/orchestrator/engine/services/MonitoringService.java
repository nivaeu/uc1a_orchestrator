package eu.niva.orchestrator.engine.services;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.quartz.CalendarIntervalScheduleBuilder;
import org.quartz.CalendarIntervalTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.orchestrator.engine.OrchestratorConfiguration;
import eu.niva.orchestrator.engine.api.MonitoringRequest;
import eu.niva.orchestrator.engine.api.MonitoringResult;
import eu.niva.orchestrator.engine.api.MonitoringResultList;
import eu.niva.orchestrator.engine.api.MonitoringTypeRequest;
import eu.niva.orchestrator.engine.api.WorkflowOrchestrator;
import eu.niva.orchestrator.engine.api.WorkflowOrchestrator.Procedure;
import eu.niva.orchestrator.engine.common.Constants.Status;
import eu.niva.orchestrator.engine.common.JarUtils;
import eu.niva.orchestrator.engine.db.MonitoringDao;
import eu.niva.orchestrator.engine.db.ProcessorDao;
import eu.niva.orchestrator.engine.db.ntt.MonitoringReq;
import eu.niva.orchestrator.engine.db.ntt.MonitoringRes;
import eu.niva.orchestrator.engine.db.ntt.MonitoringType;
import eu.niva.orchestrator.engine.db.ntt.Processor;
import eu.niva.orchestrator.engine.scheduler.MonitoringJob;
import eu.niva.orchestrator.engine.workflow.CreateProcessRequest;
import eu.niva.orchestrator.engine.workflow.ProcessResponse;
import eu.niva.orchestrator.engine.workflow.WorkflowDto;
import eu.niva.orchestrator.engine.workflow.WorkflowDto.Variable;
import eu.niva.orchestrator.sdk.MonitoringParams;

public class MonitoringService {

    private ProcessorDao processorDao;
    private MonitoringDao monitoringDao;
    private Client client;
    private OrchestratorConfiguration config;
    private ObjectMapper objectMapper;
    private static final Logger log = LoggerFactory.getLogger(MonitoringService.class);
    private ModelMapper modelMapper;
    private VelocityEngine velocityEngine;
    private SchedulerFactory schedulerFactory;
    private Scheduler schedulerMonitoring;

    public MonitoringService(ProcessorDao processorDao, MonitoringDao monitoringDao, ObjectMapper objectMapper, Client client, OrchestratorConfiguration config)
	throws RuntimeException {
	super();
	this.config = config;
	this.client = client;
	this.processorDao = processorDao;
	this.monitoringDao = monitoringDao;
	this.objectMapper = objectMapper;
	this.modelMapper = new ModelMapper();
	Converter<Boolean, Integer> converter = ctx -> ctx.getSource() == true ? 1 : 0;
	this.modelMapper.addConverter(converter);
	modelMapper.typeMap(WorkflowOrchestrator.Procedure.class, WorkflowDto.Procedure.class).addMappings(mapper -> {
	    mapper.map(src -> src.getProcessor(), WorkflowDto.Procedure::setClass_name);
	});

	Properties p = new Properties();
	p.setProperty("resource.loader", "class");
	p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
	this.velocityEngine = new VelocityEngine();
	this.velocityEngine.init(p);

	try {
	    schedulerFactory = new StdSchedulerFactory("quartz.properties");
	    schedulerMonitoring = schedulerFactory.getScheduler();
	} catch (SchedulerException e) {
	    throw new RuntimeException("ERROR INITIALIZING QUARTZ SCHEDULER", e);
	}

    }

    public Response createMonitoringRequest(MonitoringRequest monitoringRequest) {
	Long registrationId;
	try {
	    MonitoringReq monitoringReq = modelMapper.map(monitoringRequest, MonitoringReq.class);
	    MonitoringType monitoringType = monitoringDao.getMonitoringTypeByName(monitoringReq.getMonitor_type_name())
		.orElseThrow(() -> new NotFoundException("Monitor Type Name not found"));
	    String jsonAttr = objectMapper.writeValueAsString(monitoringRequest.getAttributes());
	    monitoringReq.setAttributes(jsonAttr);
	    registrationId = monitoringDao.insertMonitoring(monitoringReq);	 
	    if (monitoringType.getIs_immediate() == 1) {
		Optional<MonitoringReq> monitoringReqObj = monitoringDao.getMonitoringRequestsByRegistrationId(registrationId);
		monitoringDao.updateStatusMonitoringRequest(registrationId, Status.IN_PROGRESS);
		try {
		    createMonitoringWorkflowProcess(monitoringType, monitoringReqObj.get());
		} finally {
		    monitoringDao.updateStatusMonitoringRequest(registrationId, Status.COMPLETED);
		}
	    } else {
		monitoringDao.updateStatusMonitoringRequest(registrationId, Status.SCHEDULED);
	    }

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while creating monitoring request, poligon ID: %s ", monitoringRequest.getPolygon_id());
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring request failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return Response.ok(registrationId).build();

    }

    public void scheduleMonitorTypesJobs() throws Exception {
	
	for (JobKey key : schedulerMonitoring.getJobKeys(GroupMatcher.jobGroupEquals("MonitoringJobs"))) {
	    schedulerMonitoring.deleteJob(key);
	}
	List<MonitoringType> monitoringTypeList = monitoringDao.getMonitoringTypes();
	for (MonitoringType monitoringType : monitoringTypeList) {
	    scheduleMonitoringRequestType(monitoringType);
	}

    }

    public void scheduleMonitoringRequestType(MonitoringType monitoringType) throws Exception {

	schedulerMonitoring.deleteJob(new JobKey(monitoringType.getMonitor_type_name(), "MonitoringJobs"));

	JobDataMap jobDataMap = new JobDataMap();
	jobDataMap.put("monitoringDao", monitoringDao);
	jobDataMap.put("monitoringService", this);
	JobDetail job = JobBuilder.newJob(MonitoringJob.class).withIdentity(monitoringType.getMonitor_type_name(), "MonitoringJobs").usingJobData(jobDataMap)
	    .usingJobData("MonitoringType", monitoringType.getMonitor_type_name()).build();

	int year = Calendar.getInstance().get(Calendar.YEAR);

	int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

	if (dayOfYear >= monitoringType.getDoy_end()) {// IF EXPIRED THEN SCHEDULE ON NEXT YEAR
	    year += 1;
	}

	Calendar calendarStart = new GregorianCalendar(year, 0, 1);
	calendarStart.add(Calendar.DAY_OF_YEAR, monitoringType.getDoy_start());

	Calendar calendarEnd = Calendar.getInstance();
	calendarEnd.set(Calendar.YEAR, year);
	calendarEnd.set(Calendar.DAY_OF_YEAR, monitoringType.getDoy_end());

	CalendarIntervalTrigger calendarIntervalTrigger = TriggerBuilder.newTrigger()
	    .withIdentity("MonitoringTrigger_" + monitoringType.getMonitor_type_name(), "MonitoringTriggers")
	    .withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule()
		// .withIntervalInSeconds(monitoringType.getFrequency()))
		.withIntervalInDays(monitoringType.getFrequency()))
	    .startAt(calendarStart.getTime()).endAt(calendarEnd.getTime()).build();

	for (JobKey key : schedulerMonitoring.getJobKeys(GroupMatcher.jobGroupEquals("MonitoringJobs"))) {
	    System.out.println(key.toString());
	}
	schedulerMonitoring.scheduleJob(job, calendarIntervalTrigger);
	schedulerMonitoring.start();

    }

    private void writeGroovy(String processorName, String connectorName) throws Exception {

	FileWriter fileWriter = new FileWriter("wf-jar/groovy/" + processorName + ".groovy", false);
	Template template = velocityEngine.getTemplate("templates/groovy.vm");
	VelocityContext vc = new VelocityContext();
	vc.put("processor", processorName);
	vc.put("connector", connectorName);
	template.merge(vc, fileWriter);
	fileWriter.close();

    }

    public MonitoringType createWorkflowJar(MonitoringTypeRequest monitoringTypeRequest) {
	
	MonitoringType monitoringType = null;
	try {
	    File jarPath = new File("wf-jar");
	    for (File file : jarPath.listFiles()) {
		if (file.isDirectory()) {
		    for (File jarfile : file.listFiles())
			jarfile.delete();
		} else {
		    file.delete();
		}
	    }
	    for (Procedure procedure : monitoringTypeRequest.getWorkflow().getProcedures()) {
		Processor processor = processorDao.findProcessorByName(procedure.getProcessor()).orElseThrow(() -> new NotFoundException(
		    String.format("Processor %s not found for workflow %s", procedure.getProcessor(), monitoringTypeRequest.getWorkflow().getCode())));
		writeGroovy(processor.getProcessor_name(), processor.getConnector_name());
	    }

	    WorkflowDto workflowDto = modelMapper.map(monitoringTypeRequest.getWorkflow(), WorkflowDto.class);
	    for (eu.niva.orchestrator.engine.workflow.WorkflowDto.Procedure proc : workflowDto.getProcedures()) {
		proc.setClass_name("groovy/" + proc.getClass_name() + ".groovy");
	    }

	    monitoringType = modelMapper.map(monitoringTypeRequest, MonitoringType.class);
	    monitoringType.setWorkflow(objectMapper.writeValueAsString(monitoringTypeRequest.getWorkflow()));
	    monitoringType.setWorkflow_code(workflowDto.getCode());

	    workflowDto.getVariables().add(new Variable("processorParams", "java.util.Map", 0, 1));
	    workflowDto.getVariables().add(new Variable("monitoringParams", "eu.niva.orchestrator.sdk.MonitoringParams", 0, 1));

	    objectMapper.writerWithDefaultPrettyPrinter().writeValue(Paths.get("wf-jar/abaco-workflow.json").toFile(), workflowDto);

	    JarUtils.buildJar(workflowDto.getCode());
	 
	    try(InputStream fis = new FileInputStream("wf-jar/" + workflowDto.getCode() + ".jar")) {
		byte[] jarByteArray = IOUtils.toByteArray(fis);
		String encodedJarString = Base64.getEncoder().encodeToString(jarByteArray);
    		String url = config.getWorkflowRestUploadUrl();
    		Response response = client.target(url).queryParam("fileName", workflowDto.getCode() + ".jar").request()
    			 .post(Entity.entity(encodedJarString, MediaType.TEXT_PLAIN_TYPE));
    		if (response.getStatus() != Response.Status.OK.getStatusCode()) {
    			 throw new Exception(String.format("Exception while uploading workflow jar file %s" + workflowDto.getCode()));
    		}
	    }

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while creating monitoring type, name: %s ", monitoringTypeRequest.getMonitor_type_name());
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring type creation failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return monitoringType;
	
    }

    public synchronized Response createMonitoringType(MonitoringTypeRequest monitoringTypeRequest) {
	Integer monitoringTypeId;
	try {
	    if (monitoringDao.getMonitoringTypeByName(monitoringTypeRequest.getMonitor_type_name()).isPresent()) {
		throw new WebApplicationException("Monitoring type name already exists", Response.Status.NOT_ACCEPTABLE);
	    }

	    MonitoringType monitoringType = createWorkflowJar(monitoringTypeRequest);

	    monitoringTypeId = monitoringDao.inTransaction(dao -> {
		Integer monTypeId = dao.insertMonitoringType(monitoringType);
		for (Procedure procedure : monitoringTypeRequest.getWorkflow().getProcedures()) {
		    dao.insertMonitoringTypeProcessors(monitoringType.getMonitor_type_name(), procedure.getProcessor());
		}
		return monTypeId;
	    });

	    scheduleMonitoringRequestType(monitoringType);

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while creating monitoring type, name: %s ", monitoringTypeRequest.getMonitor_type_name());
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring type creation failed", Response.Status.INTERNAL_SERVER_ERROR);
	}

	log.debug(String.format("Monitor Type  %s created succesfully with id :  %s", monitoringTypeRequest.getMonitor_type_name(), monitoringTypeId));

	return Response.ok("Monitor Type created succesfully").build();
    }

    public Response updateMonitoringType(MonitoringTypeRequest monitoringTypeRequest) {
	
	monitoringDao.getMonitoringTypeByName(monitoringTypeRequest.getMonitor_type_name())
	    .orElseThrow(() -> new NotFoundException(String.format("Monitor type name not found: %s", monitoringTypeRequest.getMonitor_type_name())));

	try {
	    MonitoringType monitoringType = modelMapper.map(monitoringTypeRequest, MonitoringType.class);

	    createWorkflowJar(monitoringTypeRequest);
	    monitoringDao.inTransaction(dao -> {
		Integer monTypeId = monitoringDao.updateMonitoringType(monitoringType);
		dao.deleteMonitoringTypeProcessors(monitoringType.getMonitor_type_name());
		for (Procedure procedure : monitoringTypeRequest.getWorkflow().getProcedures()) {
		    dao.insertMonitoringTypeProcessors(monitoringType.getMonitor_type_name(), procedure.getProcessor());
		}
		return monTypeId;
	    });

	    scheduleMonitoringRequestType(monitoringType);

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while  updating Monitoring type, name: %s ", monitoringTypeRequest.getMonitor_type_name());
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring type update failed", Response.Status.INTERNAL_SERVER_ERROR);
	}

	log.debug(String.format("Monitor Type  %s updated succesfully ", monitoringTypeRequest.getMonitor_type_name()));

	return Response.ok("Monitor Type updated succesfully").build();
	
    }

    public Response deleteMonitoringType(String monitorTypeName) throws Exception {
	
	Integer deleteId = null;
	try {
	    List<MonitoringReq> monitoringReqList = monitoringDao.getMonitoringRequestsByTypeName(monitorTypeName);
	    if (monitoringReqList != null && !monitoringReqList.isEmpty()) {
		throw new WebApplicationException(
		    String.format("Cannot delete monitor type %s while is in use in %s Monitor Requests", monitorTypeName, monitoringReqList.size()),
		    Response.Status.NOT_ACCEPTABLE);
	    }
	    deleteId = monitoringDao.deleteMonitoringType(monitorTypeName);
	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exception while deleting Monitoring type : %s ", monitorTypeName);
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring type delete failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	if (deleteId == null) {
	    throw new NotFoundException(String.format("Monitor type name not found: %s", monitorTypeName));
	}

	schedulerMonitoring.deleteJob(new JobKey(monitorTypeName, "MonitoringJobs"));

	return Response.ok("Monitor Type deleted succesfully").build();
	
    }

    @SuppressWarnings("unchecked")
    public ProcessResponse createMonitoringWorkflowProcess(MonitoringType monitoringType, MonitoringReq monitoringReq) throws Exception {
	
	ProcessResponse res = null;
	try {
	    CreateProcessRequest createProcessRequest = new CreateProcessRequest();
	    createProcessRequest.setReturn_global_vars(true);
	    createProcessRequest.setWorkflow_code(monitoringType.getWorkflow_code());
	    createProcessRequest.setScope(null);
	    WorkflowOrchestrator orchestratorWfDto = objectMapper.readValue(monitoringType.getWorkflow(), WorkflowOrchestrator.class);
	    Map<String, Object> processorsMap = new HashMap<String, Object>();
	    createProcessRequest.setGlobal_vars(new HashMap<String, Object>());
	    for (Procedure procedure : orchestratorWfDto.getProcedures()) {
		Processor processor = processorDao.findProcessorByName(procedure.getProcessor())
		    .orElseThrow(() -> new NotFoundException(String.format("Processor %s not founde ", procedure.getProcessor())));
		Map<String, Object> procParamsMap = objectMapper.readValue(processor.getProcessor_params(), Map.class);
		processorsMap.put(processor.getProcessor_name(), procParamsMap);
	    }

	    createProcessRequest.getGlobal_vars().put("processorParams", processorsMap);

	    Map<String, Object> monitReqAttribMap = null;

	    if (monitoringReq.getAttributes() != null) {
		monitReqAttribMap = objectMapper.readValue(monitoringReq.getAttributes(), Map.class);
	    }

	    OffsetDateTime offsetDateTime = monitoringReq.getRegistration_date().toInstant().atOffset(ZoneOffset.UTC);
	    MonitoringParams monitoringParams = new MonitoringParams(monitoringReq.getPolygon_id(), monitoringReq.getGeometry(),
		monitoringReq.getReference_year(), offsetDateTime, monitReqAttribMap);

	    createProcessRequest.getGlobal_vars().put("monitoringParams", monitoringParams);

	    String url = config.getWorkflowRestProcessUrl();
	    Response response = client.target(url).request().post(Entity.entity(createProcessRequest, MediaType.APPLICATION_JSON_TYPE));
	    res = response.readEntity(ProcessResponse.class);

	    res.getGlobal_vars().keySet().removeIf(key -> key.contains("processorParams"));// REMOVE NOT NEEDED RESULT DATA
	    res.getGlobal_vars().keySet().removeIf(key -> key.contains("monitoringParams"));

	    JsonNode nodeGlobals = objectMapper.valueToTree(res.getGlobal_vars());
	    JsonNode nodeMeta = objectMapper.valueToTree(res.getCurrent_node_metadata());

	    MonitoringRes monitoringResult = new MonitoringRes(null, monitoringReq.getRegistration_id(), null, nodeMeta.toString(), nodeGlobals.toString());

	    monitoringDao.insertMonitoringResult(monitoringResult);

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    String message = String.format("Exce ption while creating Monitoring Workflow process: %s ", monitoringType.getMonitor_type_name());
	    log.error(message, e);
	    throw new WebApplicationException("Monitoring type delete failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return res;
	
    }

    public MonitoringResultList getMonitoringResultList(long registrationId) throws Exception {
	
	MonitoringResultList monitoringResultList = new MonitoringResultList();
	try {
	    @SuppressWarnings("unused")
	    MonitoringReq monitoringReq = monitoringDao.getMonitoringRequestsByRegistrationId(registrationId)
		.orElseThrow(() -> new NotFoundException(String.format("No Monitoring Request found with registrationId  %s ", registrationId)));
	    List<MonitoringRes> monitoringResList = monitoringDao.getMonitoringResultList(registrationId);
	    if (monitoringResList.isEmpty()) {
		throw new NotFoundException(String.format("No Monitoring Result found on registrationId  %s ", registrationId));
	    }

	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
	    monitoringResultList.setExecution_date(new HashMap<Long, String>());
	    monitoringResultList.setRegistration_id(registrationId);
	    for (MonitoringRes monitoringRes : monitoringResList) {
		monitoringResultList.getExecution_date().put(monitoringRes.getResult_id(),
		    formatter.format(monitoringRes.getExecution_date().toLocalDateTime()));
	    }

	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    log.error("Exception while getting monitoring result list", e);
	    throw new WebApplicationException("Getting monitoring result list failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return monitoringResultList;
	
    }

    @SuppressWarnings("unchecked")
    public MonitoringResult getMonitoringResult(long resultId) throws Exception {
	
	MonitoringResult monitoringResult = new MonitoringResult();
	try {

	    MonitoringRes monitoringRes = monitoringDao.getMonitoringResult(resultId)
		.orElseThrow(() -> new NotFoundException(String.format("Monitoring result not found with registrationId  %s ", resultId)));

	    monitoringResult = modelMapper.map(monitoringRes, MonitoringResult.class);

	    if (monitoringRes.getResult() != null) {
		Map<String, Object> mapResult = objectMapper.readValue(monitoringRes.getResult(), Map.class);
		monitoringResult.setResult(mapResult);
	    }
	    if (monitoringRes.getMetadata() != null) {
		Map<String, Object> mapMeta = objectMapper.readValue(monitoringRes.getMetadata(), Map.class);
		monitoringResult.setMetadata(mapMeta);
	    }
	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    log.error("Exception while getting monitoring result", e);
	    throw new WebApplicationException("Getting monitoring result failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return monitoringResult;
	
    }

    public WorkflowOrchestrator getMonitoringTypeWorkflow(String monitoringTypeName) {
	
	MonitoringType monitoringType = null;
	WorkflowOrchestrator workflowOrchestrator;
	try {
	    monitoringType = monitoringDao.getMonitoringTypeByName(monitoringTypeName).orElseThrow(() -> new NotFoundException("Monitor Type Name not found"));
	    workflowOrchestrator = objectMapper.readValue(monitoringType.getWorkflow(), WorkflowOrchestrator.class);
	} catch (WebApplicationException ex) {
	    throw ex;
	} catch (Exception e) {
	    log.error("Exception while getting monitoring types", e);
	    throw new WebApplicationException("Getting monitoring types failed", Response.Status.INTERNAL_SERVER_ERROR);
	}
	return workflowOrchestrator;
	
    }

    public List<MonitoringType> getMonitoringTypes() {
	
	List<MonitoringType> monitoringTypes;
	try {
	    monitoringTypes = monitoringDao.getMonitoringTypes();
	} catch (Exception e) {
	    log.error("Exception while getting monitoring types", e);
	    throw new WebApplicationException("Getting monitoring types failed", Response.Status.INTERNAL_SERVER_ERROR);
	}

	return monitoringTypes;
	
    }



//  public void scheduleReloadMonitorTypesNewYear() throws Exception{
//	
//	schedulerMonitoring.deleteJob(new JobKey("SchedulingJob", "SchedulingJobs"));
//
//	JobDataMap jobDataMap = new JobDataMap();
//	jobDataMap.put("monitoringService", this);
//      CronTrigger cronTrigger = TriggerBuilder.newTrigger()
//          .withIdentity("SchedulingTrigger", "SchedulingTriggers")          
//          .withSchedule(CronScheduleBuilder.cronSchedule("0 17 12 21 MAY ? *"))//TODO 0 1 0 1 JAN ? *
//          .build();
//      
//	JobDetail job = JobBuilder.newJob(SchedulingJob.class)
//	    .withIdentity("SchedulingJob", "SchedulingJobs")
//	    .usingJobData(jobDataMap)
//	    .build();
//	
//	schedulerMonitoring.scheduleJob(job, cronTrigger);
//	schedulerMonitoring.start();
//  }
}

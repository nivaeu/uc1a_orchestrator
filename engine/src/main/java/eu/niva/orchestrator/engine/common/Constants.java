package eu.niva.orchestrator.engine.common;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class Constants {

    public static class Status {
	public final static String COMPLETED = "COMPLETED";
	public final static String SCHEDULED = "SCHEDULED";
	public final static String IN_PROGRESS = "IN PROGRESS";

    }

}

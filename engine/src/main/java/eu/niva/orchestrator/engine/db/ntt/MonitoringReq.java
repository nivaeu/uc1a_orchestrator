package eu.niva.orchestrator.engine.db.ntt;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Date;

import org.locationtech.jts.geom.Geometry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringReq {
	private String polygon_id;
	private Geometry geometry;
	private String srs;
	private String monitor_type_name;
	private Integer reference_year;
	private String status;
	private String attributes;
	private Long registration_id;
	private Date registration_date;

}

package eu.niva.orchestrator.engine;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.client.Client;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.SqlLogger;
import org.jdbi.v3.core.statement.StatementContext;
import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import eu.niva.orchestrator.engine.common.WKTGeometryDeserializer;
import eu.niva.orchestrator.engine.common.WKTGeometrySerializer;
import eu.niva.orchestrator.engine.db.MonitoringDao;
import eu.niva.orchestrator.engine.db.ProcessorDao;
import eu.niva.orchestrator.engine.jdbi.PgJdbiPlugin;
import eu.niva.orchestrator.engine.resources.MonitoringResources;
import eu.niva.orchestrator.engine.resources.ProcessorResources;
import eu.niva.orchestrator.engine.services.MonitoringService;
import eu.niva.orchestrator.engine.services.ProcessorService;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

public class OrchestratorApplication extends Application<OrchestratorConfiguration> {

    private static final Logger log = LoggerFactory.getLogger(OrchestratorApplication.class);

    public static void main(String[] args) throws Exception {
	new OrchestratorApplication().run(args);
    }

    @Override
    public String getName() {
	return "Orchestrator Engine";
    }

    @Override
    public void initialize(Bootstrap<OrchestratorConfiguration> bootstrap) {
	bootstrap.addBundle(new JdbiExceptionsBundle());
	bootstrap.addBundle(new MigrationsBundle<OrchestratorConfiguration>() {
	    @Override
	    public PooledDataSourceFactory getDataSourceFactory(OrchestratorConfiguration configuration) {
		return configuration.getDataSourceFactory();
	    }
	});

	// bootstrap.addCommand(new ShutdownCommand());

	SubstitutingSourceProvider provider = new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
	    new EnvironmentVariableSubstitutor(false));
	bootstrap.setConfigurationSourceProvider(provider);

    }

    @Override
    public void run(OrchestratorConfiguration config, Environment environment) throws Exception {
	ObjectMapper objectMapper = environment.getObjectMapper();
	configure(objectMapper);

	// environment.admin().addTask(new ShutdownTask());

	final JdbiFactory factory = new JdbiFactory();
	final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "orchestratorDB");
	if (config.isEnableSqlLogging()) {
	    SqlLogger sqlLogger = new SqlLogger() {
		@Override
		public void logAfterExecution(StatementContext context) {
		    log.info("sql {}, parameters {}, timeTaken {} ms", context.getRenderedSql(), context.getBinding().toString(),
			context.getElapsedTime(ChronoUnit.MILLIS));
		}
	    };
	    jdbi.setSqlLogger(sqlLogger);
	}
	jdbi.installPlugin(new PgJdbiPlugin());
	SimpleModule module = new SimpleModule("custom-deserializers", Version.unknownVersion());
	module.addSerializer(Geometry.class, new WKTGeometrySerializer());
	module.addDeserializer(Geometry.class, new WKTGeometryDeserializer());
	objectMapper.registerModule(module);
	// Resources
	ProcessorDao processorDao = jdbi.onDemand(ProcessorDao.class);
	MonitoringDao monitorDao = jdbi.onDemand(MonitoringDao.class);

	final Client client = new JerseyClientBuilder(environment).using(config.getJerseyClientConfiguration()).build(getName());

	MonitoringService monitoringService = new MonitoringService(processorDao, monitorDao, environment.getObjectMapper(), client, config);
	ProcessorService processorService = new ProcessorService(processorDao, monitoringService, environment.getObjectMapper());

	monitoringService.scheduleMonitorTypesJobs();

	environment.jersey().register(new ProcessorResources(processorService));
	environment.jersey().register(new MonitoringResources(monitoringService));

	initOpenApi(environment);
    }

    private void configure(ObjectMapper objectMapper) {
	objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    private void initOpenApi(Environment environment) {
	OpenAPI oas = new OpenAPI();

	oas.info(new Info().title("ABACO orchestrator engine").contact(new Contact().email("info@abacogroup.eu")));

	SwaggerConfiguration oasConfig = new SwaggerConfiguration().openAPI(oas).prettyPrint(true)
	    .resourcePackages(Stream.of("eu.niva.orchestrator.engine.resources").collect(Collectors.toSet()));

	environment.jersey().register(new OpenApiResource().openApiConfiguration(oasConfig));
    }

}

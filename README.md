# NIVA - uc1a orchestrator

This repo contains the backend project for the NIVA uc1a orchestrator, the project is managed as a set of MAVEN modules; 
please see README.md files in the sub directories for module specific information.

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Compile and package

To create the libraries and the engine api artifacts:

    mvn clean install

## Installation
The installable artifact is generated as part of the orchestrator-engine sub module; please refer to the README.md file in that module for further instructions.
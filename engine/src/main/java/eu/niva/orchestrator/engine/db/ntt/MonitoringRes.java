package eu.niva.orchestrator.engine.db.ntt;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringRes
{
    	@NotNull private Long result_id;
	@NotNull private Long registration_id;
	@NotNull private Timestamp execution_date;
	@NotNull private String result;
	@NotNull private String metadata;
}

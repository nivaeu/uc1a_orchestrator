package eu.nive.orchestrator.engine.test;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.niva.orchestrator.engine.OrchestratorApplication;
import eu.niva.orchestrator.engine.OrchestratorConfiguration;
import eu.niva.orchestrator.engine.api.MonitoringRequest;
import eu.niva.orchestrator.engine.api.MonitoringTypeRequest;
import eu.niva.orchestrator.engine.api.ProcessorRequest;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.util.Duration;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

@ExtendWith(DropwizardExtensionsSupport.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrchestratorIntegrationTest {

    private static final String CONFIG_PATH_TEST = ResourceHelpers.resourceFilePath("test_config.yml");
    private static final String CONFIG_PATH_TEST_DB = ResourceHelpers.resourceFilePath("test_config_db.yml");
    private static final String MIGRATE_PATH_TEST = "migrations.xml";
    private static Client client = null;
    private final static JerseyClientConfiguration jerseyClientConfiguration = new JerseyClientConfiguration();

    private static String baseUrl;

    private static final DropwizardAppExtension<OrchestratorConfiguration> APP_EXT_DB = new DropwizardAppExtension<>(OrchestratorApplicationTest.class,
	CONFIG_PATH_TEST_DB);

    public static final DropwizardTestSupport<OrchestratorConfiguration> APP_TEST = new DropwizardTestSupport<OrchestratorConfiguration>(
	OrchestratorApplication.class, CONFIG_PATH_TEST);

    @BeforeAll
    private static void migrate() throws Exception {
	// CREATE H2 IN MEMORY DB
	DataSourceFactory dataSourceFactory = APP_EXT_DB.getConfiguration().getDataSourceFactory();
	Properties info = new Properties();
	info.setProperty("user", dataSourceFactory.getUser());
	info.setProperty("password", dataSourceFactory.getPassword());
	org.h2.jdbc.JdbcConnection h2Conn = new org.h2.jdbc.JdbcConnection(dataSourceFactory.getUrl(), info);
	JdbcConnection conn = new JdbcConnection(h2Conn);
	Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(conn);
	Liquibase liquibase = new Liquibase(MIGRATE_PATH_TEST, new ClassLoaderResourceAccessor(), database);
	String ctx = null;
	liquibase.update(ctx);
	APP_TEST.before(); // START APP
	jerseyClientConfiguration.setTimeout(Duration.minutes(1));
	baseUrl = "http://localhost:" + APP_TEST.getLocalPort() + "/orchestrator/api/v1";
	client = new JerseyClientBuilder(APP_TEST.getEnvironment()).using(jerseyClientConfiguration).build("test client");
	liquibase.close();

    }

    @AfterAll
    public static void afterClass() {
	APP_TEST.after(); // STOP APP
    }

    @Test
    @Order(1)
    public void whenCreateProcessor_thenReturn200_OK() {
	ProcessorRequest processorRequest = new ProcessorRequest();
	processorRequest.setConnector_name("connector1");
	processorRequest.setProcessor_name("NumberOfPixels");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("param1", 1);
	map.put("param2", "param2");
	processorRequest.setProcessor_params(map);

	Response response = client.target(baseUrl + "/processor").request().post(Entity.entity(processorRequest, MediaType.APPLICATION_JSON_TYPE));

	assertEquals(response.getStatus(), Status.OK.getStatusCode());

    }

    @Test
    @Order(2)
    public void whenCreateProcessor_withExistingName_thenReturn406_NOT_ACCEPTABLE() {
	ProcessorRequest processorRequest = new ProcessorRequest();
	processorRequest.setConnector_name("connector1");
	processorRequest.setProcessor_name("NumberOfPixels");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("param1", 1);
	map.put("param2", "param2");
	processorRequest.setProcessor_params(map);

	Response response = client.target(baseUrl + "/processor").request().post(Entity.entity(processorRequest, MediaType.APPLICATION_JSON_TYPE));

	assertEquals(response.getStatus(), Status.NOT_ACCEPTABLE.getStatusCode());

    }

    @Test
    @Order(3)
    public void whenCreateMonitoringType_withWrongProcessorName_thenReturn404_NOTFOUND() {
	MonitoringTypeRequest monitoringTypeRequest = null;
	try (InputStream in = getClass().getClassLoader().getResourceAsStream("monitorType.json")) {
	    ObjectMapper mapper = new ObjectMapper();
	    monitoringTypeRequest = mapper.readValue(in, MonitoringTypeRequest.class);

	} catch (Exception e) {
	    throw new RuntimeException(e);
	}

	Response response = client.target(baseUrl + "/monitoring/type").request().post(Entity.entity(monitoringTypeRequest, MediaType.APPLICATION_JSON_TYPE));

	assertEquals(response.getStatus(), Status.NOT_FOUND.getStatusCode());

    }
       
    
    @Test
    @Order(5)
    public void whenGetProcessorList_thenReturnOneItem() {

	@SuppressWarnings("unchecked")
	List<ProcessorRequest> list = client.target(baseUrl + "/processor").request().get( List.class);

	assertEquals(list.size(), 1);

    }
    
    
   
    @Test
    @Order(6)
    public void whenUpdateProcessor_thenReturn200_OK() {
	ProcessorRequest processorRequest = new ProcessorRequest();
	processorRequest.setConnector_name("connector1");
	processorRequest.setProcessor_name("NumberOfPixels");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("param99", 999);
	map.put("param222", "param222");
	processorRequest.setProcessor_params(map);

	Response response = client.target(baseUrl + "/processor").request().put(Entity.entity(processorRequest, MediaType.APPLICATION_JSON_TYPE));

	assertEquals(response.getStatus(), Status.OK.getStatusCode());

    }

    @Test
    @Order(7)
    public void whenDeleteProcessor_thenReturn200_OK() {

	Response response = client.target(baseUrl + "/processor/NumberOfPixels").request().delete();

	assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());

    }

    @Test
    @Order(8)
    public void whenCreateMonitoringRequest_withWrongMonitorTypeName_thenReturn404_NOTFOUND() {
	MonitoringRequest monitoringRequest = new MonitoringRequest();
	GeometryFactory gf = new GeometryFactory();
	Polygon geo = gf.createPolygon(
	    new Coordinate[] { new Coordinate(-9, -2), new Coordinate(-9, -1), new Coordinate(-8, -1), new Coordinate(-8, -2), new Coordinate(-9, -2) });
	monitoringRequest.setGeometry(geo);
	monitoringRequest.setSrs("srsValue");
	monitoringRequest.setReference_year(2021);
	monitoringRequest.setPolygon_id("id13");
	monitoringRequest.setMonitor_type_name("wrong_monitor_type");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("attr1", 1);
	map.put("attr2", "val2");
	monitoringRequest.setAttributes(map);

	Response response = client.target(baseUrl + "/monitoring").request().post(Entity.entity(monitoringRequest, MediaType.APPLICATION_JSON_TYPE));

	assertEquals(response.getStatus(), Status.NOT_FOUND.getStatusCode());

    }


}

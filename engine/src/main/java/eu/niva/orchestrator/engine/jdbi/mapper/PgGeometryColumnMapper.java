package eu.niva.orchestrator.engine.jdbi.mapper;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import org.jdbi.v3.core.mapper.ColumnMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.postgis.PGgeometry;

public class PgGeometryColumnMapper implements ColumnMapper<Geometry>
{
	private Map<Integer, LinkedList<BorrowedWKTReader>> WKT_READERS = new ConcurrentHashMap<>();
	
	private Consumer<Connection> geometrySupportConsumer;
	
	public PgGeometryColumnMapper(Consumer<Connection> geometrySupportConsumer)
	{
		this.geometrySupportConsumer = geometrySupportConsumer;
	}

	@Override
	public Geometry map(ResultSet rs, int columnNumber, StatementContext ctx) throws SQLException
	{
		geometrySupportConsumer.accept(rs.getStatement().getConnection());
		
		Object obj = rs.getObject(columnNumber);
		org.postgis.Geometry geom;
		if (obj instanceof org.postgis.Geometry)
		{
			geom = (org.postgis.Geometry) obj;
		}
		else if (obj instanceof PGgeometry)
		{
			geom = ((PGgeometry) obj).getGeometry();
		}
		else
		{
			throw new SQLException("Unsupported geometry object type, got: " + obj.getClass());
		}
		
		// TODO: Implement a more efficient conversion?
		//       maybe a switch on geom.getTypeString() and create ad-hoc methods to convert to
		//       locationtech geoms without converting to/from WKT...
		
		StringBuffer sb = new StringBuffer(1024);
		geom.outerWKT(sb, false);
		try (BorrowedWKTReader wktReader = borrowWktReader(geom.srid))
		{
				return wktReader.read(sb.toString());
		}
		catch (ParseException e)
		{
			throw new SQLException(e);
		}
	}

	private BorrowedWKTReader borrowWktReader(int srid)
	{
		LinkedList<BorrowedWKTReader> pool = WKT_READERS.computeIfAbsent(srid, s -> new LinkedList<>());
		synchronized (pool)
		{
			if (pool.isEmpty())
				return new BorrowedWKTReader(srid, new WKTReader(new GeometryFactory(new PrecisionModel(), srid)));
			return pool.pop();
		}
	}
	
	private class BorrowedWKTReader implements AutoCloseable
	{
		private Integer srid;
		private WKTReader reader;

		public BorrowedWKTReader(Integer srid, WKTReader reader)
		{
			this.srid = srid;
			this.reader = reader;
		}

		public Geometry read(String str) throws ParseException
		{
			return reader.read(str);
		}
		
		@Override
		public void close()
		{
			WKT_READERS.get(srid).add(this);
		}
	}

}

package eu.niva.orchestrator.engine.jdbi;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Connection;
import java.sql.SQLException;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.spi.JdbiPlugin;
import org.postgis.PGgeometry;
import org.postgresql.jdbc.PgConnection;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import eu.niva.orchestrator.engine.jdbi.argument.PgGeometryArgumentFactory;
import eu.niva.orchestrator.engine.jdbi.mapper.PgGeometryColumnMapper;

public class PgJdbiPlugin implements JdbiPlugin
{
	
	private Cache<Integer, Integer> seenConnections;
	
	public PgJdbiPlugin()
	{
		init();
	}

	private void init()
	{
		seenConnections = Caffeine.newBuilder()
			.maximumSize(200)
			.build();
	}
	
	@Override
	public void customizeJdbi(Jdbi jdbi) throws SQLException
	{
		jdbi.registerArgument(new PgGeometryArgumentFactory(this::geometrySupportConsumer));
		jdbi.registerColumnMapper(new PgGeometryColumnMapper(this::geometrySupportConsumer));
	}
	
	private void geometrySupportConsumer(Connection cnn)
	{
		try
		{
			PgConnection pgCnn = (PgConnection) cnn;
			int hash = pgCnn.hashCode();
			if (seenConnections.getIfPresent(hash) == null)
			{
				synchronized (pgCnn)
				{
					pgCnn.addDataType("geometry", PGgeometry.class);
					seenConnections.put(hash, hash);
				}
			}
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}

	}
}

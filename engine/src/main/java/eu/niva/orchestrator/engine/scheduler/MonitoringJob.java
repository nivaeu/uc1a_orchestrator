package eu.niva.orchestrator.engine.scheduler;

/*-
 * #%L
 * engine
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import org.jdbi.v3.core.result.ResultIterator;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.niva.orchestrator.engine.common.Constants.Status;
import eu.niva.orchestrator.engine.db.MonitoringDao;
import eu.niva.orchestrator.engine.db.ntt.MonitoringReq;
import eu.niva.orchestrator.engine.db.ntt.MonitoringType;
import eu.niva.orchestrator.engine.services.MonitoringService;

public class MonitoringJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(MonitoringJob.class);

    public MonitoringJob() {
	super();
    }

    /// service result per data + lista registrationid
    /// change

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	Map<String, Object> wm = dataMap.getWrappedMap();
	String monitoringTypeName = dataMap.getString("MonitoringType");
	MonitoringDao monitoringDao = (MonitoringDao) wm.get("monitoringDao");
	MonitoringService monitoringService = (MonitoringService) wm.get("monitoringService");
	MonitoringType monitoringType = monitoringDao.getMonitoringTypeByName(monitoringTypeName)
	    .orElseThrow(() -> new JobExecutionException(monitoringTypeName + " not found"));
	int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
	if (dayOfYear > monitoringType.getDoy_end()) {// TO SKYP FIRST TRIGGER WHEN DAYSTART IS < DAY_OF_YEAR
	    return;
	}
	String exitStatus = dayOfYear >= monitoringType.getDoy_end() ? Status.COMPLETED : Status.SCHEDULED;
	Calendar calendarCurrent = new GregorianCalendar(); // (2021, 0, 1)

	LocalDate dateCurrent = calendarCurrent.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	int year = calendarCurrent.get(Calendar.YEAR);

	LocalDate dateStart = null;
	Calendar calendarStart = new GregorianCalendar(year, 0, 1);
	calendarStart.add(Calendar.DAY_OF_YEAR, monitoringType.getDoy_start());
	dateStart = calendarStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

	Calendar calendarEnd = new GregorianCalendar(year, 0, 1);
	calendarEnd.add(Calendar.DAY_OF_YEAR, monitoringType.getDoy_end());
	// calendarEnd.add(Calendar.DAY_OF_YEAR, 10);
	LocalDate dateEnd = calendarEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

	if (!dateCurrent.isAfter(dateStart) || !dateCurrent.isEqual(dateStart) || !dateCurrent.isBefore(dateEnd) || !dateCurrent.isEqual(dateEnd)) {
	    year += 1;
	}

	log.debug(String.format("START OF JOB EXECUTION MONITORING TYPE  %s", monitoringType.getMonitor_type_name()));
	try (ResultIterator<MonitoringReq> monitoringReqList = monitoringDao.getMonitoringRequestsByStatusAndYear(Status.SCHEDULED, year)) {
	    while (monitoringReqList.hasNext()) {
		MonitoringReq monitoringReq = monitoringReqList.next();
		try {

		    monitoringDao.updateStatusMonitoringRequest(monitoringReq.getRegistration_id(), Status.IN_PROGRESS);

		    log.debug(String.format("Started monitoring execution for request registration id %s ", monitoringReq.getRegistration_id()));

		    monitoringService.createMonitoringWorkflowProcess(monitoringType, monitoringReq);

		    log.debug(String.format("Succesfully monitoring executed for request registration id %s ", monitoringReq.getRegistration_id()));
		} catch (Exception e) {
		    log.error(String.format("Error in execution monitoring for request registration id %s ", monitoringReq.getRegistration_id()));
		} finally {
		    monitoringDao.updateStatusMonitoringRequest(monitoringReq.getRegistration_id(), exitStatus); // ? error status ?
		    if (exitStatus.equals(Status.COMPLETED)) {
			monitoringService.scheduleMonitoringRequestType(monitoringType);
		    }
		}
	    }
	    log.debug(String.format("END OF JOB EXECUTION MONITORING TYPE  %s", monitoringType.getMonitor_type_name()));
	} catch (Exception e) {
	    throw new JobExecutionException(e.getMessage(), e);
	}
    }
}
